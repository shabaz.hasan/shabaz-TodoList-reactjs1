import React from 'react';



export default function Todoitem(props) {

  function removeTodo() {
    props.removeTodo(props.id);
  }
  function dragStart() {
    props.dragStart(props.id);

  }
  function drop() {
    props.dropItem(props.id);
  }
  function allowdrag(e) {
    e.preventDefault();
  }
  function checkMark() {
    props.check(props.id);
  }
  if (props.todo.status) {
    return (
      <div className="todoWrapper" draggable="true" onDragOver={allowdrag} onDragStart={dragStart} onDrop={drop}>
        <span onClick={checkMark}>{props.todo.text}</span>
        <button className="removeTodo" onClick={removeTodo}>X</button>
      </div>
    )
  }
  else {
    return (
      <div className="todoWrapper" draggable="true" onDragOver={allowdrag} onDragStart={dragStart} onDrop={drop}>
        <span style={{ textDecoration: "line-through" }} onClick={checkMark}>{props.todo.text}</span>
        <button className="removeTodo" onClick={removeTodo}>X</button>
      </div>
    )
  }

}
