import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import TodoInput from './components/todoinput';
import Todoitem from './components/todoItem';



class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      todos: [{ id: 0, text: "i am going to bed", status: true }, { id: 1, text: "i am sleeping", status: true }, { id: 2, text: "hello", status: true }],
      nextId: 3


    }
    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
    this.dragStart = this.dragStart.bind(this);
    this.dropItem = this.dropItem.bind(this);
    this.checkMark = this.checkMark.bind(this);
    this.previd = null;
  }

  addTodo(todoText) {
    let todos = this.state.todos.slice();
    todos.push({
      id: this.state.nextId,
      text: todoText,
      status: true
    })
    this.setState({
      todos: todos,
      nextId: ++this.state.nextId
    })
  }
  removeTodo(id) {
    this.setState({
      todos: this.state.todos.filter((todo, index) => todo.id != id)
    })
  }

  dragStart(id) {
    console.log("start" + id)
    this.previd = id;
  }
  dropItem(id) {
    console.log(id);
    let copy = [];
    let newtodos = this.state.todos;
    var data = newtodos[this.previd];
    for (var i = 0; i < newtodos.length; i++) {
      copy.push(newtodos[i]);
    }
    copy.splice(this.previd, 1);
    copy.splice(id, 0, data);
    console.log(copy);
    copy = copy.map((todo, i) => {
      todo.id = i;
      return todo
    });
    console.log(copy);
    this.setState({ todos: copy });
  }
  checkMark(id) {
    let newtodos = this.state.todos.map(todo => {
      if (todo.id == id && todo.status == false) {
        todo.status = true;
      }
      else if (todo.id == id && todo.status == true) {
        todo.status = false;
      }
      return todo;
    })
    this.setState({ todos: newtodos })
  }



  render() {
    return (
      <div className="App">
        <div className="todo-wrapper">
          <Header />
          <TodoInput todoText="" addTodo={this.addTodo} />
          <ul>
            {
              this.state.todos.map((todo) => {
                return <Todoitem todo={todo} key={todo.id} id={todo.id} removeTodo={this.removeTodo} dragStart={this.dragStart} dropItem={this.dropItem} check={this.checkMark} />
              })
            }
          </ul>
        </div>
      </div>
    );
  }

}

export default App;
